#!/usr/bin/env node

const fs = require("fs");
const { execSync } = require("child_process");

const homepage = process.argv[2];
const packagePath = `${__dirname}/package.json`

if (homepage) {
  fs.copyFileSync(packagePath, packagePath+'.bak')
  const package = JSON.parse(fs.readFileSync(packagePath))
  package.homepage = homepage
  fs.writeFileSync(packagePath, JSON.stringify(package))
}

execSync("yarn build", { stdio: 'inherit' })

if (homepage) {
  fs.renameSync(packagePath+'.bak', packagePath)
}
