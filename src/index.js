import 'regenerator-runtime/runtime'

import 'bootstrap/dist/css/bootstrap.css';
import './assets/style/main.css';

import React from "react";
import ReactDOM from "react-dom";
import App from './components/App';

ReactDOM.render(<App/>, document.getElementById("app"));
