import React, { useState } from 'react';
import XLSX from 'xlsx';

function Logs({ logs=[] }) {
  const [visible, setVisible] = useState(false);
  const logLines = logs.map(l => <li key={l.ts.toISOString()}>{l.line}</li>)
  const arrow = <i className={"arrow " + (visible ? "down" : "right") }/>
  return <div>
    <h5 onClick={() => setVisible(!visible)}>Logs {arrow}</h5>
    { visible ? 
      <ul>{logLines}</ul> :
      null }
  </div>
}

function Progress({ progress }) {
  return <div className="progress">
    <div className="progress-bar progress-bar-animated" role="progressbar" aria-valuenow={progress} aria-valuemin="0" aria-valuemax="100" style={{ width: progress+"%"}}></div>
  </div>
}

function Columns({ unexpectedCols, missingCols }) {
  const missing = missingCols.length === 0 ?
    <div className="col-6"/> :
    <div className="col-6">
      <h4>Missing Columns</h4>
      { missingCols.map((c, i) => <span key={i} className="badge rounded-pill bg-warning text-dark mr5">{c}</span>)}
    </div>
  const unexpected = unexpectedCols.length === 0 ?
    <div className="col-6"/> :
    <div className="col-6">
      <h4>Unexpected Columns</h4>
      { unexpectedCols.map((c, i) => <span key={i} className="badge rounded-pill bg-warning text-dark mr5">{c}</span>)}
    </div>
  return <div className="row">
    {missing}
    {unexpected}
  </div>
}

function Table({ wbOut }) {
  const table = { __html: XLSX.utils.sheet_to_html(wbOut.Sheets['Sample'], { header: "", footer: ""}) }
  return <div className="preview mt20" dangerouslySetInnerHTML={ table }/>
}

function DownloadButton({ wbOut, wbOutName }) {
  const onClick = () => XLSX.writeFile(wbOut, wbOutName, { sheet: "Filtered", bookType: "csv" });
  return <button type="button" className="btn btn-secondary mt10" onClick={onClick}>Download</button>
}

function Result({ file }) {
  const { fileName, status="waiting", progress=0, wbOut, wbOutName, unexpectedCols=[], missingCols=[], logs=[] } = file;
  const header = status === "complete" ?
    <h3>{fileName}</h3> :
    <h3>{fileName} <span className="small-text">({status})</span></h3>
  return <div className="mt20">
    { header }
    <Logs logs={logs}/>
    { ["waiting", "running"].includes(status) ? <Progress progress={progress}/> : null }
    { ["complete", "error"].includes(status) ? <Columns unexpectedCols={unexpectedCols} missingCols={missingCols}/> : null }
    { ["complete", "error"].includes(status) ? <Table wbOut={wbOut}/> : null }
    { status === "complete" ? <DownloadButton wbOut={wbOut} wbOutName={wbOutName}/> : null }
  </div>
}

export default (props) => {
  const { files } = props;
  if (!Object.keys(files).length) return null;
  return <div className="mt20">
    <h2>Results</h2>
    { Object.keys(files).map(fileName => <Result key={fileName} file={{ fileName, ...files[fileName] }}/>) }
  </div>
}