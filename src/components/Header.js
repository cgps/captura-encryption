import React from 'react';
import logo from '../assets/img/captura/captura-500.png'

export default function() {
  return <header className="pt20 pb20">
    <img style={{ width: 500, height: 157 }} src={ logo }/>
  </header>
}