import React from 'react';
import { expectedCols } from '../filter';

export default function() {

  const columns = expectedCols.map((c, i) => <span key={i} className="badge rounded-pill bg-warning text-dark mr5">{c}</span>)

  return <div id="instructions">
    <h1>Data obfuscation tool</h1>
    <p>Hashing <mark>PATIENT_ID</mark> in Excel via SHA-256 algorithm for CAPTURA (developed by Hyeong-won Seo, BDM, EPIC, IVI, 2020-12-10)</p>
    <ul>
      <li>Choose a spreadsheet file</li>
      <li>Column <mark>PATIENT_ID</mark> and <mark>AGE_in_YEAR</mark> should remain on the header/first row.</li>
      <li>The file should include the following columns: <br/>{columns}</li>
      <li><mark>PATIENT_ID</mark> will be obfuscated</li>
      <li>All other columns will be removed</li>
    </ul>
  </div>
}