import React, { useEffect, useState } from 'react';

import Header from './Header';
import Instructions from './Instructions';
import Controls from './Controls';
import Result from './Result';
import Footer from './Footer';

import useWorker from '../hooks/useWorker';

import { readFile } from '../utils';

export default function() {
  const [files, setFiles] = useState({});

  function updateFileData({ fileName, ...data }) {
    setFiles(files => {
      return { ...files, [fileName]: { ...files[fileName], ...data }}
    });
  }
  function log(fileName, line) {
    setFiles(files => {
      const update = files[fileName] || {};
      return { ...files, [fileName]: { ...update, logs: [...update.logs || [], { line, ts: new Date() }]}}
    })
  }
  
  function handler({ type, fileName, ...data }) {
    switch (type) {
      case "log":
        log(fileName, data.message);
        break
      case "progress":
        updateFileData({ fileName, ...data, status: "running" })
        break;
      case "complete":
        updateFileData({ fileName, ...data, status: "complete", progress: 100 })
        break;
      case "columns":
        updateFileData({ fileName, ...data })
        break;
      case "error":
        const { message } = data;
        updateFileData({ fileName, status: "error", message })
    }
  }

  const postMessage = useWorker(handler);
  
  useEffect(() => {
    const numberRunning = Object.values(files).filter(({ status }) => status === "running").length
    if (numberRunning === 0) {
      const fileName = Object.keys(files).find(fileName => files[fileName].status === "waiting");
      if (fileName === undefined) return;
      const { file } = files[fileName];
      readFile(file).then(fileContents => {
        postMessage({ type: "process", fileName, fileContents })
        updateFileData({ fileName, status: "running" })
      })
    }
  }, [files])

  function addFiles(e) {
    const update = {};
    for (const file of e.target.files) {
      const fileName = file.name;
      update[fileName] = { status: "waiting", file }
    }
    setFiles({ ...files, ...update })
  }

  return <React.Fragment>
    <Header/>
    <Instructions/>
    <Controls addFiles={addFiles}/>
    <Result files={files}/>
    <Footer/>
  </React.Fragment>
}