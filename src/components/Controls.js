import React from 'react';

export default function({ addFiles }) {
  return <div className="controls">
   <form>
      <label className="btn btn-captura-primary">
        <input type="file" onChange={addFiles} multiple/>
        Select Spreadsheets
      </label>
    </form>
  </div>
}