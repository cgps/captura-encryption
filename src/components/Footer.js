import React from 'react';
import logo from '../assets/img/captura/ivi-300.png'

export default function() {
  return <footer className="pt20 pb20">
    <img src={ logo }/>
  </footer>
}