// 
// File handling
// 

export function readFile(file) {
  const fileName = file.name;

  if (typeof (FileReader) == "undefined") {
    return Promise.resolve()
  }

  //Validate whether File is valid Excel file.
  const regex = /(.xlsx|.xlsm|.xls|.csv)$/;
  if (!regex.test(fileName.toLowerCase())) {
    return Promise.resolve()
  }


  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    //For Browsers other than IE.
    if (reader.readAsBinaryString) {
      reader.onload = function(e) {
        return resolve(e.target.result);
      };
      reader.readAsBinaryString(file);
    } else {
      //For IE Browser.
      reader.onload = function(e) {
        var data = "";
        var bytes = new Uint8Array(e.target.result);
        for (var i = 0; i < bytes.byteLength; i++) {
          data += String.fromCharCode(bytes[i]);
        }
        return resolve(data);
      };
      reader.readAsArrayBuffer(file);
    }
  })
};