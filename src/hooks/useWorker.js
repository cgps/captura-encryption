/* eslint import/no-webpack-loader-syntax: off */
import { useEffect, useRef } from 'react';
import Worker from "worker-loader!../worker.js";

export default function(handler) {
  const workerRef = useRef();
  const handlerRef = useRef();

  useEffect(() => {
    const worker = new Worker();
    workerRef.current = worker;
    return () => worker.terminate();
  }, [])

  useEffect(() => {
    handlerRef.current = handler;
  }, [handler])

  useEffect(
    () => {
      workerRef.current.onmessage = (e) => handlerRef.current(e.data);
    }, []
  );
  return (message) => workerRef.current.postMessage(message);
}