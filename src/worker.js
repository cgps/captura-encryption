import XLSX from 'xlsx';
import { v4 as uuidv4 } from 'uuid';
import sha256 from 'js-sha256';

import { expectedCols } from './filter';

// 
// Utils
// 

function log(fileName, message) {
  postMessage({ type: "log", fileName, message })
}

function remove(arr, v) {
  const idx = arr.indexOf(v);
  if (idx == -1) return;
  arr.splice(idx, 1)
}

//
// Parsing and filtering
// 

function isSmallNumber(v) {
  return /^\d{1,4}$/.test((v || "").toString())
}

const AGE_CAP = 70;
function limitYearOfBirth(v) {
  if (!isSmallNumber(v)) return "";
  const now = new Date().getFullYear();
  const yearCap = now - AGE_CAP;
  return v <= yearCap ? `<=${yearCap}` : v;
}

function limitAge(v) {
  if (!isSmallNumber(v)) return "";
  return v >= AGE_CAP ? `>=${AGE_CAP}` : v
}

const secret = uuidv4();
const obfuscate = (c) => {
  const before = c.v
  const after = '__' + sha256(`${secret}${before}`).slice(0, 16) + '__'
  return { v: after, w: after, t: "s" }
}

function buildColumnLookup(ws) {
  const unexpectedCols = [];
  const missingCols = [...expectedCols]
  const columnLookup = {};

  const range = XLSX.utils.decode_range(ws['!ref']); // get the range

  for (let colNum = range.s.c; colNum <= range.e.c; colNum++) {
    const addr = XLSX.utils.encode_col(colNum) + XLSX.utils.encode_row(0);
    const header = ws[addr].v;
    if (!expectedCols.includes(header)) {
      unexpectedCols.push(header);
      continue;
    };
    columnLookup[header] = colNum;
    remove(missingCols, header);
  }

  function cell(rowNum, column) {
    const colNum = columnLookup[column];
    if (colNum === undefined) return undefined;
    const addr = XLSX.utils.encode_col(colNum) + XLSX.utils.encode_row(rowNum);
    return ws[addr];
  }

  return { cell, missingCols, unexpectedCols, foundCols: expectedCols.filter(_ => columnLookup[_] !== undefined) }
}

function ProcessExcel({ fileName, workbook }) {
  //Fetch the name of First Sheet.
  const firstSheet = workbook.SheetNames[0];
  const ws = workbook.Sheets[firstSheet];

  log(fileName, `Building the lookup`)
  const { cell, missingCols, unexpectedCols, foundCols } = buildColumnLookup(ws);
  postMessage({ type: "columns", fileName, missingCols, unexpectedCols, foundCols })

  const sheetData = []
  log(fileName, `Obfuscating ${fileName}`);
  
  const range = XLSX.utils.decode_range(ws['!ref']); // get the range
  const numRows = range.e.r
  for (var rowNum = 1; rowNum <= numRows; rowNum++) {
    const row = {};
    for (const key of expectedCols) {
      switch (key) {
        case 'PATIENT_ID':
          row[key] = obfuscate(cell(rowNum, key))
          break;
        case 'YEAR_of_BIRTH':
          row[key] = limitYearOfBirth(cell(rowNum, key).v);
          break;
        case 'AGE_in_YEAR':
          row[key] = limitAge(cell(rowNum, key).v);
          break;
        default:
          row[key] = cell(rowNum, key);
      }
      if (rowNum % 100 === 0) {
        postMessage({ type: "progress", fileName, progress: Math.floor(100 * rowNum / numRows)})
      }
    }
    sheetData.push(row);
  }

  const wbOut = XLSX.utils.book_new();
  const wsOut = XLSX.utils.json_to_sheet(sheetData, { header: foundCols })
  const sample = XLSX.utils.json_to_sheet(sheetData.slice(0, 10), { header: foundCols })
  XLSX.utils.book_append_sheet(wbOut, wsOut, 'Filtered')
  XLSX.utils.book_append_sheet(wbOut, sample, 'Sample')
  log(fileName, `Obfuscated ${fileName}`);

  return wbOut
};

//
// Worker handler
//

function processFile(fileName, fileContents) {
  if (fileContents === undefined) {
    return postMessage({ type: "error", fileName, message: "fileContents are undefined" })
  };
  log(fileName, `Read ${fileName}`);

  postMessage({ type: "progress", fileName, progress: 0})

  const wbIn = XLSX.read(fileContents, {
    type : 'binary'
  });
  log(fileName, `Parsed ${fileName}`)

  const wbOut = ProcessExcel({ fileName, workbook: wbIn });

  const wbOutName = fileName.replace(/\..{2,4}$/, "_converted.csv")
  return postMessage({ type: "complete", fileName, wbOut, wbOutName })
}

onmessage = function(e){
  const { type, fileName, fileContents } = e.data;
  switch (type) {
    case "process":
      processFile(fileName, fileContents);
      break;
    default:
      log(fileName, `unexpected message ${type}: keys = ${Object.keys(e.data).join(", ")}`)
  }
}
